function Graph()
{
    this.n		= 0;
    this.nodes		= {};
    this.edges		= {};
}

Graph.prototype.Build = function (json) {
    var obj = jQuery.parseJSON(json);
    
    this.n = obj.size;
    
    for (var i in obj.nodes) {
        this.nodes[obj.nodes[i].id] = obj.nodes[i];
    }
    
    for (var i in obj.edges) {
        this.edges[obj.edges[i].id] = obj.edges[i];
    }
    
    
/*
    for (var n in this.nodes) {
        console.log("Nodes: " + n + " -> " + this.nodes[n].name); 
    }
    
    for(var i in this.edges) {
        console.log("Edges: " + this.edges[i].inN + " -> " + this.edges[i].outN);
    }
    
    console.log("size: " + this.n);
    */
    
    
}
