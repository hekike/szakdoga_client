/*
http://en.wikipedia.org/wiki/Force-based_algorithms_(graph_drawing)
*/

function Grapher(graphInput) {
    
    // class variables
    this.vertices = {};
    this.repulsion = 900;
    this.attraction = 0.09;
    this.damping = 0.7;
    this.stable = false;
    this.dragged = null;
    this.is3D = false;
    
    // Graph
    this.graph = new Graph
    this.graph.Build(graphInput);

    
    // set up initial node positions randomly
    var x, y ,z;
    for (var i in this.graph.nodes) {
        
        var nodeId = this.graph.nodes[i].id;
        
        x = -400 + Math.random() * 800;
        y = -400 + Math.random() * 800;
        z = -400 + Math.random() * 800;
        
        v = new Vertex(x, y, this.is3D ? z : -100);
        this.vertices[nodeId] = v;
    }

}

// until total_kinetic_energy is less than some small number
Grapher.prototype.Iterate = function() {
    
    // if stable do nothing
    if(this.stable) {
        return this.stable;
    }
    
    var nodeId, edgeId, node2Id;   
    var b, a;

    // for each node
    for (var i in this.graph.nodes) {

        nodeId = this.graph.nodes[i].id;
        
        a = this.vertices[nodeId];
        a.f.x = a.f.y = a.f.z = 0; 

        // for each other node
        for (var j in this.graph.nodes) {
            node2Id = this.graph.nodes[j].id;
                     
            if(nodeId != node2Id) {
                b = this.vertices[node2Id];

                // net-force := net-force + Coulomb_repulsion( this_node, other_node )
                var c = this.repulsion / ( (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z) );
                a.f.x += c * (a.x - b.x);
                a.f.y += c * (a.y - b.y);
                a.f.z += c * (a.z - b.z)
            }
        }
    }

    // for each spring connected to this node
    for (var i in this.graph.edges) {

        edgeId = this.graph.edges[i].id;
        

        var inEdge = this.vertices[this.graph.edges[edgeId].inN];
        var outEdge = this.vertices[this.graph.edges[edgeId].outN];
        
        // net-force := net-force + Hooke_attraction( this_node, spring )
        inEdge.f.x += this.attraction * (outEdge.x - inEdge.x);
        inEdge.f.y += this.attraction * (outEdge.y - inEdge.y);
        inEdge.f.z += this.attraction * (outEdge.z - inEdge.z);
        
        outEdge.f.x += this.attraction * (inEdge.x - outEdge.x);
        outEdge.f.y += this.attraction * (inEdge.y - outEdge.y);
        outEdge.f.z += this.attraction * (inEdge.z - outEdge.z);
    
    }
    
    var total_kinetic_energy = 0;
    for (var i in this.graph.nodes) {  

        nodeId = this.graph.nodes[i].id;
 
        if(a = this.vertices[nodeId], a != this.dragged) {

            // without damping, it moves forever
            // this_node.velocity := (this_node.velocity + timestep * net-force) * damping
            a.v.x = (a.v.x + a.f.x) * this.damping;
            a.v.y = (a.v.y + a.f.y) * this.damping;
            a.v.z = (a.v.z + a.f.z) * this.damping;

            // this_node.position := this_node.position + timestep * this_node.velocity
            a.x += a.v.x;
            a.y += a.v.y;
            a.z += a.v.z;

            // total_kinetic_energy
            total_kinetic_energy += Math.abs(a.v.x) + Math.abs(a.v.y) + Math.abs(a.v.z);
    
        }
    }

    // total_kinetic_energy is less than some small number
    return this.stable = total_kinetic_energy < 0.5;
    
};

// drag start
Grapher.prototype.SetDragged = function(i) {

    this.dragged = this.vertices[i];
    this.stable = false;

};
  
// drag in progress  
Grapher.prototype.MoveDragged = function(x, y) {

    if(this.dragged) {

        this.dragged.x = x;
        this.dragged.y = y;

        this.stable = false;

    }

};
      
// drag end  
Grapher.prototype.StopDragging = function() {

    this.dragged = null

};

// switch 3D <-> 2D
Grapher.prototype.Switch3D = function() {

    this.is3D = !this.is3D;

    // 3D, 2D
    if(this.is3D) {

        for (var i in this.graph.nodes) {
            var nodeId = this.graph.nodes[i].id;    
            this.vertices[nodeId].z = -80 + Math.random() * 160;  
        }
        
        this.stable = false;
        
    } else {

        for (var i in this.graph.nodes) {
            var nodeId = this.graph.nodes[i].id;   
            
            this.vertices[nodeId].z = -100;
            this.vertices[nodeId].f.z = -100;
            this.vertices[nodeId].v.z = -100;
        }
        this.stable = false;
    }

}