// Graph
var url = "jsonpp.txt";
var grapher, graph;

// Visual
var container, stats;
var camera, scene, projector, renderer;
var nodes = [], edges = [], plane;
var dtov = {};

var mouse = new THREE.Vector2(), offset = new THREE.Vector3(), INTERSECTED, SELECTED;
var cameaMove = false;
var defaultCameraFov = 70;
var cameraFov = defaultCameraFov;

var prevMouseX, prevMouseY;

//JSON lekérés indítás
$(document).ready(function() {
    
    $('#slider_attraction').slider({
        range: "min",
        value:0.09,
        min: 0.001,
        max: 0.7,
        step: 0.01,
        slide: function(event, ui) { 
            grapher.attraction = ui.value;
            grapher.stable = false;
        }
    });
    
    $('#slider_repulsion').slider({
        range: "min",
        value:900,
        min: 10,
        max: 3000,
        step: 50,
        slide: function(event, ui) { 
            grapher.repulsion = ui.value;
            grapher.stable = false;
        }
    });
    
    
    $.get(url, function(data) {
        graphInput = data;

        init();
        animate();
    });
    
    $("#switch3d").click(function() {
        if(grapher) {
            grapher.Switch3D();
        }
    });
    
});


function init(){
    
    // Graph
    
    grapher = new Grapher(graphInput)
    grapher.Iterate();
    graph = grapher.graph;


    // Visual
	
    container = document.getElementById("canvas_container");
    
    scene = new THREE.Scene();
    
    camera = new THREE.PerspectiveCamera(cameraFov, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 800;
    scene.add(camera);
      
	
    //Nodes
    for (var i in graph.nodes) {
        
        var nodeId = graph.nodes[i].id;
       
        //Color
        var bgcolor = (graph.nodes[i].depth / 100) * 0xffffff;
        var textColor = "0x000000";
    
        var posX = grapher.vertices[nodeId].x;
        var posY = grapher.vertices[nodeId].y;
        var posZ = grapher.vertices[nodeId].z;

        var geometry = new THREE.CubeGeometry( 20, 20, 20 );

        var newNode = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( {
            color: bgcolor,
            overdraw: true
        } ) );
        
        newNode.position.x = posX;
        newNode.position.y = posY;
        newNode.position.z = posZ;
 
        var text3d = new THREE.TextGeometry( graph.nodes[i].name, {

            size: 10,
            height: 1,
            curveSegments: 1,
            font: "roboto"

        });

        text3d.computeBoundingBox();
        var centerOffset = -0.5 * ( text3d.boundingBox.max.x - text3d.boundingBox.min.x );

        var textMaterial = new THREE.MeshBasicMaterial( {
            color: textColor, 
            overdraw: true
        } );
        
        
        text = new THREE.Mesh( text3d, textMaterial );

        text.doubleSided = false;

        text.position.x = centerOffset;
        text.position.y = -30;
        text.position.z = 10;

        var parent = new THREE.Object3D();
        parent.add( text );

        scene.add( parent );
        
        newNode.text = parent;
        
        // TEXT END
        
        newNode.material.ambient = newNode.material.color;
        newNode.dynamic = true;
		
        //Edges
        newNode.edges = [];
        
        //Id
        newNode.visId = nodes.length;
        newNode.dataId = nodeId;
        dtov[nodeId] = newNode.visId;
        
        newNode.castShadow = true;
        newNode.receiveShadow = true;
			
        scene.add(newNode);
        
        nodes.push(newNode);
	
    }
    
    //Edges
    var edgeMat = new THREE.LineBasicMaterial({
        color: 0x7d7d7d, //0x010068, //0xffba18
        opacity: 1.0,
        linewidth: 2
    } );
    
    for (var i in graph.edges) {
        
        var edgeId = graph.edges[i].id;
                     
        var nodeInId = graph.edges[edgeId].inN;
        var nodeOutId = graph.edges[edgeId].outN;
            
        var edgeGeom = new THREE.Geometry();
        
        var vertex1 = new THREE.Vertex(nodes[dtov[nodeInId]].position);
        var vertex2 = new THREE.Vertex(nodes[dtov[nodeOutId]].position);
        
        edgeGeom.vertices.push(vertex1);
        edgeGeom.vertices.push(vertex2);
                 
        var drawingLine = new THREE.Line( edgeGeom , edgeMat );
        
        drawingLine.position.z -= 1;
        
        //Store In, Out node ID
        drawingLine.inNodeId = nodeInId;
        drawingLine.outNodeId = nodeOutId;
        
        nodes[dtov[nodeInId]].edges.push(drawingLine);
        nodes[dtov[nodeOutId]].edges.push(drawingLine);
			
        //Meshes additionally need also dynamic flag enabled (to keep internal typed arrays).
        drawingLine.dynamic = true;
			
        scene.add(drawingLine );
			
        edges.push(drawingLine);
      
    }
    
    // Plane	 
    plane = new THREE.Mesh(new THREE.PlaneGeometry(2000, 2000, 8, 8), new THREE.MeshBasicMaterial({
        color: 0xFF0000,
        opacity: 0.25,
        transparent: true,
        wireframe: true
    }));
    
    plane.lookAt(camera.position);
    plane.visible = false;
    scene.add(plane);
    
    projector = new THREE.Projector();
    
    //Lights
    if (Detector.webgl ) {
        
        //WebGL
        scene.add(new THREE.AmbientLight(0x606060));

        var light = new THREE.SpotLight(0xffffff, 1.5);
        light.castShadow = true;

        light.shadowCameraNear = 200;
        light.shadowCameraFar = camera.far;
        light.shadowCameraFov = 50;

        light.shadowBias = -0.00022;
        light.shadowDarkness = 0.5;

        light.shadowMapWidth = 1024;
        light.shadowMapHeight = 1024;
        
        scene.add(light);
    } else { 
        
        var light = new THREE.SpotLight(0xffffff, 1.5);
        scene.add(light);
         
        var ambientLight = new THREE.AmbientLight( 0x606060 );
        scene.add( ambientLight );

        directionalLight = new THREE.DirectionalLight( 0xffffff );
        directionalLight.position.x = Math.random() - 0.5;
        directionalLight.position.y = Math.random() - 0.5;
        directionalLight.position.z = Math.random() - 0.5;
        directionalLight.position.normalize();
        scene.add( directionalLight );

        var directionalLight = new THREE.DirectionalLight( 0x808080 );
        directionalLight.position.x = Math.random() - 0.5;
        directionalLight.position.y = Math.random() - 0.5;
        directionalLight.position.z = Math.random() - 0.5;
        directionalLight.position.normalize();
        scene.add( directionalLight );


    }    
   
    
    // Renderer
    if (Detector.webgl ) {
        renderer = new THREE.WebGLRenderer({
            antialias: true
        });
        renderer.sortObjects = false;
        renderer.shadowMapEnabled = true;
        renderer.shadowMapSoft = true;
  
    }
    else {
        renderer = new THREE.CanvasRenderer();
    }

    renderer.setSize(window.innerWidth, window.innerHeight);

    container.appendChild(renderer.domElement);
    
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    container.appendChild(stats.domElement);

    
}
$(document).ready(function() {
    $("#canvas_container").mousemove(function(event) {
    
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

        var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
        projector.unprojectVector(vector, camera);
    
        var ray = new THREE.Ray(camera.position, vector.subSelf(camera.position).normalize());
    
        var intersects;
    
        //If dragged thand move
    
        if (SELECTED) {
    
            intersects = ray.intersectObject(plane);
            if(intersects[0]) {
                SELECTED.position.copy(intersects[0].point.subSelf(offset));
                grapher.MoveDragged(SELECTED.position.x, SELECTED.position.y);
            }

            return;
        
        } 

      
        //Select for drag
    
        intersects = ray.intersectObjects(nodes);
    
        if (intersects.length > 0) {
    
            if (INTERSECTED != intersects[0].object) {
        
                if (INTERSECTED) {
                    INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
                }
            
                INTERSECTED = intersects[0].object;
                INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
                INTERSECTED.material.color.setHex( 0xff0000 );
            
                plane.position.copy(INTERSECTED.position);
                grapher.SetDragged(INTERSECTED.dataId);
 
            }
        
            $(".profile").show();   
            $(".profile h3").html(graph.nodes[INTERSECTED.dataId].name);
            $(".profile .id").html("database id: " + graph.nodes[INTERSECTED.dataId].id);
            $(".profile .type").html("type: " + graph.nodes[INTERSECTED.dataId].type);
        
            var dataHtml = "";
            for(var key in graph.nodes[INTERSECTED.dataId].data) {
                dataHtml += key + ": " + graph.nodes[INTERSECTED.dataId].data[key] + "<br/>";
            }
            $(".profile .data").html(dataHtml);
        
            container.style.cursor = 'pointer';
        
        }
        else {
    
            if (INTERSECTED) {
                INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
            }
        
            INTERSECTED = null;
        
            container.style.cursor = 'auto';
        
            $(".profile").hide();
        
        }
    
        
        //Camera move
        if(cameaMove) {
            container.style.cursor = 'move';
        
            var deltaX = prevMouseX - event.clientX;
            var deltaY = prevMouseY - event.clientY;
        
            deltaX *= (cameraFov / defaultCameraFov) * 1.2;
            deltaY *= (cameraFov / defaultCameraFov) * 1.2;
        
            camera.position.x += deltaX;
            camera.position.y -= deltaY;
        
            prevMouseX = event.clientX;
            prevMouseY = event.clientY;

        }
    
    
    });

    $("#canvas_container").mousedown(function(event) {

        var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
        projector.unprojectVector(vector, camera);
    
        var ray = new THREE.Ray(camera.position, vector.subSelf(camera.position).normalize());
    
        var intersects = ray.intersectObjects(nodes);
    
        if (intersects.length > 0) {
    
            SELECTED = intersects[0].object;
        
            intersects = ray.intersectObject(plane);
            offset.copy(intersects[0].point).subSelf(plane.position);
        
            container.style.cursor = 'move';
        } else {
            cameaMove = true;
        
            prevMouseX = event.clientX;
            prevMouseY = event.clientY;
        }
    });

    $("#canvas_container").mouseup(function(event) {
    
        if (INTERSECTED) {
    
            plane.position.copy(INTERSECTED.position);
        
            SELECTED = null;
        
        }
    
        container.style.cursor = 'auto';
    
        //Grapher
        grapher.StopDragging();
    
        //Camera set
        cameaMove = false;
    
    });

    $('#canvas_container').mousewheel(function(event, delta, deltaX, deltaY) {

        cameraFov -= deltaY * 2.0;
        if(cameraFov < 1) {
            cameraFov = 1;
        } else if(cameraFov > 150) {
            cameraFov = 150;
        }
        camera.projectionMatrix = THREE.Matrix4.makePerspective( cameraFov, window.innerWidth / window.innerHeight, 1, 10000 );

    });

});

function animate() {

    requestAnimationFrame(animate);
    
    render();
    stats.update();
    
}


function render() {
    var i;
  
    //drag
    if(SELECTED) {	
        for(i = 0; i < SELECTED.edges.length; i++) {	
            SELECTED.edges[i].geometry.__dirtyVertices = true;				
        }		
    }
    
    // iterate
    grapher.Iterate();
    if(!grapher.stable) {
        
        for (i in graph.nodes) {
            // console.log("(x,y,z) : ("  + grapher.vertices[i].x + "," + grapher.vertices[i].y + "," + grapher.vertices[i].z + ")");
            
            var nodeDataId = graph.nodes[i].id;
            var nodeVisId = dtov[nodeDataId];
            
            nodes[nodeVisId].position.x = nodes[nodeVisId].text.position.x = grapher.vertices[nodeDataId].x;
            nodes[nodeVisId].position.y = nodes[nodeVisId].text.position.y = grapher.vertices[nodeDataId].y;
            nodes[nodeVisId].position.z = nodes[nodeVisId].text.position.z = grapher.vertices[nodeDataId].z;
            
        }
        
        for(i in edges) {
            edges[i].geometry.__dirtyVertices = true;			
        }
    }

    renderer.render(scene, camera);
    
}
