function Grapher(graphInput){
    this.vertices = [];
    this.repulsion = 900;
    this.attraction = 0.06;
    this.damping = 0.7;
    this.stable = false;
    this.dragged = null;
    
    // Graph
    this.graph = new Graph
    this.graph.Build(graphInput);

    
    // set up initial node positions randomly
    var x, y ,z;
    for(i=0; i<this.graph.n; i++) {
        
        x = -400 + Math.random() * 800;
        y = -400 + Math.random() * 800;
        z = -400 + Math.random() * 800;
        
        var v = new Vertex(x, y, z);
        this.vertices.push(v);
    }

}


Grapher.prototype.Iterate=function() {
    // if stable do nothing
    if(this.stable) {
        return this.stable;
    }
    
    //this.stable = false;
    
    var b, a;
    for(i = 0; i < this.graph.n; i++) {
        a = this.vertices[i];
        for(j = a.f.x = a.f.y = a.f.z = 0; j < this.graph.n; j++) {
            if(i != j) {
                b = this.vertices[j];
                var c = this.repulsion / ( (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z) );
                a.f.x += c * (a.x - b.x);
                a.f.y += c * (a.y - b.y);
                a.f.z += c * (a.z - b.z)
            }
        }
    }
    for(i=0; i<this.graph.edgesl.length; i++) {
        
        b = this.vertices[this.graph.edgesl[i]];
        a = this.vertices[this.graph.edgesr[i]];
        
        a.f.x += this.attraction * (b.x - a.x);
        a.f.y += this.attraction * (b.y - a.y);
        a.f.z += this.attraction * (b.z - a.z);
        
        b.f.x += this.attraction * (a.x - b.x);
        b.f.y += this.attraction * (a.y - b.y);
        b.f.z += this.attraction * (a.z - b.z);
    
    }
   
    for(i=b=0; i<this.graph.n; i++) {
        if(a = this.vertices[i], a != this.dragged) {
            // without damping, it moves forever
            a.v.x = (a.v.x + a.f.x) * this.damping;
            a.v.y = (a.v.y + a.f.y) * this.damping;
            a.v.z = (a.v.z + a.f.z) * this.damping;
            
            b += Math.abs(a.v.x) + Math.abs(a.v.y) + Math.abs(a.v.z);
            a.x += a.v.x;
            a.y += a.v.y;
            a.z += a.v.z;
            
            
        }
    }
    return this.stable = b < 0.5;
    
};

Grapher.prototype.SetDragged=function(i){

    this.dragged = this.vertices[i];
    this.stable = false;

};
    
Grapher.prototype.MoveDragged=function(x, y){
    if(this.dragged) {
        this.dragged.x = x;
        this.dragged.y = y;
        this.stable=false;
    }
};
        
Grapher.prototype.StopDragging=function(){
    this.dragged=null
};
