// Graph
var grapher, graph;
var graphInput = "16:1-2,2-3,4-5,5-6,7-8,8-9,1-4,4-7,2-5,5-8,3-6,6-9,3-10,6-11,9-12,9-14,8-15,7-16,10-11,11-12,12-13,13-14,14-15,15-16";


// Visual
var container, stats;
var camera, scene, projector, renderer;
var nodes = [], edges = [], plane;

var mouse = new THREE.Vector2(), offset = new THREE.Vector3(), INTERSECTED, SELECTED;
var cameaMove = false;
var cameraFov = 70;

init();
animate();

function init(){
    // Graph
    grapher = new Grapher(graphInput)
    grapher.Iterate();
    graph = grapher.graph;
   
    /*
    for (var n in graph.neibs) {
        console.log("Neibs: " + n + " -> " + graph.neibs[n]); 
    }
    
    for(var el in graph.edgesl) {
        console.log("Edges left: " + el + " -> " + graph.edgesl[el]);
    }
    
    for(var er in graph.edgesr) {
        console.log("Edges right: " + er + " -> " + graph.edgesr[er]);
    }
    */
	
	
    // Visual
	
    container = document.createElement('div');
    document.body.appendChild(container);
    
    scene = new THREE.Scene();
    
    camera = new THREE.PerspectiveCamera(cameraFov, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 800;
    scene.add(camera);
    
  
    //Lights
    scene.add(new THREE.AmbientLight(0x505050));
      
    var light = new THREE.SpotLight(0xffffff, 1.5);
    light.position.set(0, 500, 2000);
    light.castShadow = true;
    
    light.shadowCameraNear = 200;
    light.shadowCameraFar = camera.far;
    light.shadowCameraFov = 50;
    
    light.shadowBias = -0.00022;
    light.shadowDarkness = 0.5;
    
    light.shadowMapWidth = 1024;
    light.shadowMapHeight = 1024;
    
    scene.add(light);
    
	
    //Nodes
    var geometry = new THREE.CubeGeometry(40, 40, 40);
    for (var i in graph.neibs) {
    
        var newNode = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({
            color: Math.random() * 0xffffff
        }));
        
        newNode.material.ambient = newNode.material.color;
        newNode.dynamic = true;
        
        newNode.position.x = grapher.vertices[i].x;
        newNode.position.y = grapher.vertices[i].y;
        newNode.position.z = grapher.vertices[i].z;
        
        /*
        newNode.rotation.x = (Math.random() * 360) * Math.PI / 180;
        newNode.rotation.y = (Math.random() * 360) * Math.PI / 180;
        newNode.rotation.z = (Math.random() * 360) * Math.PI / 180;
        
        newNode.scale.x = Math.random() * 2 + 1;
        newNode.scale.y = Math.random() * 2 + 1;
        newNode.scale.z = Math.random() * 2 + 1;
        */
		
        //Edges
        newNode.edges = [];
        
        newNode.id = nodes.length;
        newNode.castShadow = true;
        newNode.receiveShadow = true;
			
        scene.add(newNode);
        
        nodes.push(newNode);
	
    }
    
    //Edges
    var edgeMat = new THREE.LineBasicMaterial({
        color: 0x000000,
        opacity: 0.6,
        blending: THREE.AdditiveBlending,
        transparent: true,
        linewidth: 3
    } );
    
    for (var edgeId in graph.edgesl) {
                     
        var nodeInId = graph.edgesl[edgeId];
        var nodeOutId = graph.edgesr[edgeId];
            
        var edgeGeom = new THREE.Geometry();
        edgeGeom.vertices.push(new THREE.Vertex(nodes[nodeInId].position));
        edgeGeom.vertices.push(new THREE.Vertex(nodes[nodeOutId].position));
                 
        var drawingLine = new THREE.Line( edgeGeom , edgeMat );
        
        //Store In, Out node ID
        drawingLine.inNodeId = nodeInId;
        drawingLine.outNodeId = nodeOutId;
        
        nodes[nodeInId].edges.push(drawingLine);
        nodes[nodeOutId].edges.push(drawingLine);
			
        //Meshes additionally need also dynamic flag enabled (to keep internal typed arrays).
        drawingLine.dynamic = true;
			
        scene.addObject(drawingLine );
			
        edges.push(drawingLine);
      
    }
	 
    // Plane	 
    plane = new THREE.Mesh(new THREE.PlaneGeometry(2000, 2000, 8, 8), new THREE.MeshBasicMaterial({
        color: 0xFF0000,
        opacity: 0.25,
        transparent: true,
        wireframe: true
    }));
    
    plane.lookAt(camera.position);
    plane.visible = false;
    scene.add(plane);
    
    projector = new THREE.Projector();
   
    
    // Renderer
    renderer = new THREE.WebGLRenderer({
        antialias: true
    });
    renderer.sortObjects = false;
    renderer.setSize(window.innerWidth, window.innerHeight);
    
    renderer.shadowMapEnabled = true;
    renderer.shadowMapSoft = true;
	
    
    container.appendChild(renderer.domElement);
    
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    container.appendChild(stats.domElement);
    
    renderer.domElement.addEventListener('mousemove', onDocumentMouseMove, false);
    renderer.domElement.addEventListener('mousedown', onDocumentMouseDown, false);
    renderer.domElement.addEventListener('mouseup', onDocumentMouseUp, false);
    renderer.domElement.addEventListener('mousewheel', onDocumentMouseWheel, false);
    
}

function onDocumentMouseMove(event){

    event.preventDefault();
    
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
   
    
    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    projector.unprojectVector(vector, camera);
    
    var ray = new THREE.Ray(camera.position, vector.subSelf(camera.position).normalize());
    
    var intersects;
    if (SELECTED) {
    
        intersects = ray.intersectObject(plane);
        if(intersects[0]) {
            SELECTED.position.copy(intersects[0].point.subSelf(offset));
            grapher.MoveDragged(SELECTED.position.x, SELECTED.position.y);
        }

        return;
        
    } 
    
    if(cameaMove) {
       // camera.position.x = -event.clientX + window.innerWidth / 2;
       // camera.position.y = event.clientY - window.innerHeight / 2;
    }
    
  
    intersects = ray.intersectObjects(nodes);
    
    if (intersects.length > 0) {
    
        if (INTERSECTED != intersects[0].object) {
        
            if (INTERSECTED) {
                INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
            }
            
            INTERSECTED = intersects[0].object;
            INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
            
            plane.position.copy(INTERSECTED.position);
            grapher.SetDragged(INTERSECTED.id);
            
            
        }
        
        container.style.cursor = 'pointer';
        
    }
    else {
    
        if (INTERSECTED) {
            INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
        }
        
        INTERSECTED = null;
        
        container.style.cursor = 'auto';
        
        
    }
    
    
}

function onDocumentMouseDown(event){

    event.preventDefault();
    
    var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    projector.unprojectVector(vector, camera);
    
    var ray = new THREE.Ray(camera.position, vector.subSelf(camera.position).normalize());
    
    var intersects = ray.intersectObjects(nodes);
    
    if (intersects.length > 0) {
    
        SELECTED = intersects[0].object;
        
        intersects = ray.intersectObject(plane);
        offset.copy(intersects[0].point).subSelf(plane.position);
        
        container.style.cursor = 'move';
    } else {
        cameaMove = true;
    }
}

function onDocumentMouseUp(event){

    event.preventDefault();
    
    if (INTERSECTED) {
    
        plane.position.copy(INTERSECTED.position);
        
        SELECTED = null;
        
    }
    
    container.style.cursor = 'auto';
    
    //Grapher
    grapher.StopDragging();
    
    //Camera set
    cameaMove = false;
    
}

function onDocumentMouseWheel( event ) {

    cameraFov -= event.wheelDeltaY * 0.01;
    camera.projectionMatrix = THREE.Matrix4.makePerspective( cameraFov, window.innerWidth / window.innerHeight, 1, 10000 );

}



function animate() {

    requestAnimationFrame(animate);
    
    render();
    stats.update();
    
}


function render() {
    var i;
  
    //drag
    if(SELECTED) {	
        for(i = 0; i < SELECTED.edges.length; i++) {	
            SELECTED.edges[i].geometry.__dirtyVertices = true;				
        }		
    }
    
    // iterate
    grapher.Iterate();
    if(!grapher.stable) {
        // console.log("#####################");
        
        for (i in graph.neibs) {
            // console.log("(x,y,z) : ("  + grapher.vertices[i].x + "," + grapher.vertices[i].y + "," + grapher.vertices[i].z + ")");
            
            nodes[i].position.x = grapher.vertices[i].x;
            nodes[i].position.y = grapher.vertices[i].y;
            nodes[i].position.z = grapher.vertices[i].z;
            
        }
        
        for(i in edges) {
            edges[i].geometry.__dirtyVertices = true;			
        }
    }

    renderer.render(scene, camera);
    
}
